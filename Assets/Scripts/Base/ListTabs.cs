using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

namespace Bingo.Base
{
    public class ListTabs : MonoBehaviour
    {
        [ShowInInspector]
        [SerializeField] private Tab[] tabs;
        private static Tab[] stabs;
        private void Awake()
        {
            stabs = tabs;
        }

        public static T GetTab<T> () where T: Tab {
            T t = null ;
            for (int i = 0; i < stabs.Length; i++) {
                if (stabs[i].GetType() == typeof(T)) {
                    t = (T)stabs[i];
                }
            }
            return t;
        }
    }
}