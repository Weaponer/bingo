using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Bingo.Base
{
    public class Tab : MonoBehaviour
    {
        [SerializeField] private GameObject panel;

        public void Open() {
            panel.SetActive(true);
            CallOpen();
        }

        protected virtual void CallOpen() {         
        }

        public void Close() {
            panel.SetActive(false);
            CallClose();
        }

        protected virtual void CallClose() { 
        }
    }
}