using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Bingo.Base
{

    public class TabsControll : MonoBehaviour
    {
        public static TabsControll Singleton { get; private set; }

        private Tab openTab;

        private void Awake()
        {
            if (Singleton)
            {
                Destroy(gameObject);
                return;
            }
            else
            {
                Singleton = this;
            }
        }

        public static void OpenTab(Tab tab) {
            if (Singleton) {
                if (Singleton.openTab)
                    Singleton.openTab.Close();
                tab.Open();
                Singleton.openTab = tab;
            }
        }

        private void OnDisable()
        {
            if (Singleton == this)
                Singleton = null;
        }
    }
}