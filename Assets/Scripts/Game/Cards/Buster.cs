using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bingo.Game.Busters;

namespace Bingo.Game
{

    public abstract class Buster : MonoBehaviour
    {
        [SerializeField] protected Sprite sprite;

        public bool Active()
        {
            return CallActive();
        }

        protected abstract bool CallActive();

        public virtual bool CheckCreate() {
            return true;
        }       

        public Sprite GetSprite()
        {
            return sprite;
        }
    }
}
