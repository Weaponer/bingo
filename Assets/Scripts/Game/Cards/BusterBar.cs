using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Bingo.Game
{
    public class BusterBar : MonoBehaviour
    {
        [SerializeField] private BusterControll busterControll;

        [SerializeField] private GameObject baseUI;

        [SerializeField] private Image bar;

        float progress = 0f;

        private void Awake()
        {
            progress = 0;
            bar.fillAmount = 0f;
        }

        public void ResetGame()
        {
            progress = 0;
            bar.fillAmount = 0f;
        }

        public void AddProgres(float progres) {
            progress += progres;
            if (progress >= 1f) {
                busterControll.CreateNewRandomBuster();
            }
            progress = Mathf.Repeat(progress, 1f);
            bar.fillAmount = progress;
        }

        public void HideUI() {
            baseUI.SetActive(false);
            busterControll.HideUI();
        }
    }
}