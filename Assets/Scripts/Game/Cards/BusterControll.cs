using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Bingo.Game
{
    public class BusterControll : MonoBehaviour
    {
       
        [SerializeField] private Buster[] prefabBusters;

        [SerializeField] private Button activeBuster;

        [SerializeField] private GameObject baseUI;

        [SerializeField] private Image background;

        List<Buster> busters = new List<Buster>();

        private void Awake()
        {
            activeBuster.onClick.AddListener(delegate { ClickToBuster(); });
            background.color = new Color(0, 0, 0, 0);
        }

        public void HideUI()
        {
            baseUI.SetActive(false);
        }

        public Buster[] GetBusters() {
            return busters.ToArray();
        }

        public void ResetGame()
        {
            busters.Clear();
            UpdateShowBuster();
        }

        public void CreateNewRandomBuster() {
            bool t = false;
            while (!t)
            {
                int r = Random.Range(0, prefabBusters.Length);
                if (prefabBusters[r].CheckCreate())
                {
                    busters.Add(prefabBusters[r]);
                    t = true;
                }
            }
            UpdateShowBuster();
        }

        private void ClickToBuster() {
            if (busters.Count > 0) {
                UseBuster();
            }
        }

        private void UseBuster() {
            bool isNormal = busters[busters.Count - 1].Active();
            if (isNormal) {
                busters.Remove(busters[busters.Count - 1]);
            }

            UpdateShowBuster();
        }

        private void UpdateShowBuster()
        {
            if (busters.Count == 0) {
                background.color = new Color(0, 0, 0, 0);
            } else  {
                background.sprite = busters[busters.Count - 1].GetSprite();
                background.color = new Color(1, 1, 1, 1);
            }
        }
    }
}