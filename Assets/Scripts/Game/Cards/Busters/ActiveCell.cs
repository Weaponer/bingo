using Bingo.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bingo.Game.Busters
{
    public class ActiveCell : Buster
    {
        [SerializeField] private Card card;
        protected override bool CallActive()
        {
            GameTab gameTab = ListTabs.GetTab<GameTab>();
            gameTab.Panel.PanelActiveCell.SetCells(card);
            gameTab.Panel.LoadPanel(gameTab.Panel.PanelActiveCell);
            return true;
        }

        public override bool CheckCreate()
        {
            return true;
        }
    }
}