using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bingo.Game.Busters
{
    public class AddTime : Buster
    {
        protected override bool CallActive()
        {
            TimerGame.AddTime(20);
            return true;
        }
    }
}