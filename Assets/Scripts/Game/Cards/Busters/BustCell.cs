using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bingo.Game.Busters
{
    public class BustCell : Buster
    {
        [SerializeField] private BusterControll busterControll;
        [SerializeField] private Card card;

        protected override bool CallActive()
        {
            card.SetClickToBust();
            return true;
        }

        public override bool CheckCreate()
        {
            bool t = true;
            int count = 0;
            Cell[] cells = card.GetCells();
            for (int i = 0; i < cells.Length; i++)
            {
                if (cells[i].IsBust && !cells[i].IsTrue)
                    count++;
            }

            Buster[] busters = busterControll.GetBusters();

            int b = 0;
            for (int i = 0; i < busters.Length;i++) {
                if (busters[i].GetType() == typeof(BustCell))
                    b++;
            }
            if (count <= b)
                t = false;

            return t;
        }
    }
}
