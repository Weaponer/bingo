using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bingo.Game.Busters
{
    public class MultiplyScore : Buster
    {
        protected override bool CallActive()
        {
            ScoreGame.AddScore(ScoreGame.GetScore());
            return true;
        }
    }
}