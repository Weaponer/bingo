using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace Bingo.Game
{
    public delegate void CompleteCard(Card card);

    public class Card : MonoBehaviour
    {
        public event CompleteCard Complete;

        [SerializeField] private Cell prefabCell;

        List<Cell> cells = new List<Cell>();

        [SerializeField] private GridLayoutGroup gridCell;

        [SerializeField] private GameObject backGroundComplte;

        [SerializeField] private BusterBar busterBar;

        [SerializeField] private Button bingoBut;

        bool takeBust;

        public void Init(float scaleFactor)
        {
            for (int i = 0; i < 5; i++)
            {
                for (int i2 = 0; i2 < 5; i2++)
                {
                    int start = i2 * 15 + 1;
                    int end = i2 * 15 + 15;

                    Cell cell = Instantiate(prefabCell, gridCell.transform);

                    bool t = false;
                    while (!t)
                    {
                        int rand = Random.Range(start, end);
                        if (cells.Where((x) => x.Word == WordsBingo.Words[i2] && x.Num == rand).FirstOrDefault() == null)
                        {
                            cell.Init(rand, WordsBingo.Words[i2]);
                            t = true;
                        }
                    }
                    cells.Add(cell);
                    cell.Click += Click;
                }
            }

            System.Random random = new System.Random(Random.Range(0, 10000));

            for (int i = 0; i < 4; i++)
            {
                bool t = false;
                while (!t)
                {
                    int r = Random.Range(0, cells.Count - 1);
                    if (!cells[r].IsBust)
                    {
                        cells[r].SetBust();
                        t = true;
                    }
                }
            }

            RectTransform rect = gridCell.GetComponent<RectTransform>();

            gridCell.cellSize *= scaleFactor;

            bingoBut.onClick.AddListener(ClickBingo);

            ActiveCell(cells[12]);
        }

        public Cell[] GetCells()
        {
            return cells.ToArray();
        }

        public void SetClickToBust()
        {
            takeBust = true;
            for (int i = 0; i < cells.Count; i++)
            {
                if (cells[i].IsBust && !cells[i].IsTrue)
                    cells[i].ActiveLight();
            }
        }

        private void ClickBingo()
        {

            for (int i = 0; i < 5; i++)
            {
                int num = 5 * i;
                if (cells[num + 0].IsTrue && cells[num + 1].IsTrue && cells[num + 2].IsTrue && cells[num + 3].IsTrue && cells[num + 4].IsTrue)
                {
                    CardComplete();
                    return;
                }
            }
            for (int i = 0; i < 5; i++)
            {
                int num = 0 + i;
                if (cells[num + 0].IsTrue && cells[num + 5].IsTrue && cells[num + 10].IsTrue && cells[num + 15].IsTrue && cells[num + 20].IsTrue)
                {
                    CardComplete();
                    return;
                }
            }

            if (cells[0].IsTrue && cells[6].IsTrue && cells[12].IsTrue && cells[18].IsTrue && cells[24].IsTrue)
            {
                CardComplete();
                return;
            }
            if (cells[4].IsTrue && cells[8].IsTrue && cells[12].IsTrue && cells[16].IsTrue && cells[20].IsTrue)
            {
                CardComplete();
                return;
            }
            if (cells[0].IsTrue && cells[4].IsTrue && cells[20].IsTrue && cells[24].IsTrue && cells[12].IsTrue)
            {
                CardComplete();
                return;
            }
        }

        private void CardComplete()
        {
            bingoBut.gameObject.SetActive(false);
            gridCell.gameObject.SetActive(false);
            busterBar.HideUI();
            backGroundComplte.SetActive(true);
            if (Complete != null)
                Complete(this);
        }

        private void Click(Cell cell)
        {

            if (!cell.IsTrue && ((cell.IsBust && takeBust) || (!cell.IsBust && !takeBust)))
            {
                if (cell.IsBust)
                {
                    for (int i = 0; i < cells.Count; i++)
                    {
                        if (cells[i].IsBust && !cells[i].IsTrue)
                            cells[i].DisableLight();
                    }
                }
                GameTab gameTab = Base.ListTabs.GetTab<GameTab>();
                int score = gameTab.Generator.GetScore(cell);
                if (score > 0)
                {
                    cell.SetTrue();
                    busterBar.AddProgres(0.7f);
                }
                ScoreGame.AddScore(score);

                takeBust = false;
            }
        }

        public void ActiveCell(Cell cell)
        {
            cell.SetTrue();
        }
    }
}