using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Bingo.Game
{
    public delegate void ClickToCell(Cell cell);
    public class Cell : MonoBehaviour
    {
        public event ClickToCell Click;

        [SerializeField] private Sprite defaultBackgroun;

        [SerializeField] private Sprite lightBackground;

        [SerializeField] private Sprite trueBackgroun;

        [SerializeField] private Image background;

        [SerializeField] private GameObject busterIcon;

        [SerializeField] private Text num;

        [SerializeField] private Button button;

        public int Num { get; private set; }
        public char Word { get; private set; }

        public bool IsTrue { get; private set; }

        public bool IsBust { get; private set; }

        public bool IsLight { get; private set; }

        public void Init(int num, char word)
        {
            Num = num;
            Word = word;
            button.onClick.AddListener(delegate { ClickButton(); });
            SetNum(Num);
            busterIcon.SetActive(false);
        }

        public void SetNum(int num)
        {
            this.num.text = num.ToString();
        }

        public void SetTrue() {
            IsTrue = true;
            UpdateBackground();
        }

        public void SetBust() {
            IsBust = true;
            busterIcon.SetActive(true);
            num.text = "";
        }

        public void ActiveLight() {
            IsLight = true;
            UpdateBackground();
        }

        public void DisableLight() {
            IsLight = false;
            UpdateBackground();
        }

        private void UpdateBackground() {
            if (IsLight)
            {
                background.sprite = lightBackground;
            }
            else if (IsTrue)
            {
                background.sprite = trueBackgroun;
            }
            else {
                background.sprite = defaultBackgroun;
            }
        }

        private void ClickButton()
        {
            if (Click != null)
                Click(this);
        }
    }
}