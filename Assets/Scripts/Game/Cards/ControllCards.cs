using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Bingo.Game;
using Bingo.Menu;

namespace Bingo.Game
{
    public class ControllCards : MonoBehaviour
    {
        [SerializeField] private Vector2 baseScale;
        [SerializeField] private float[] scaler;

        [SerializeField] private GridLayoutGroup baseGridCards;

        [SerializeField] private Card prefabCard;

        List<Card> cards = new List<Card>();

        int countCards = 0;
        public void CreateCards(int num)
        {
            DestroyCards();
            baseGridCards.cellSize = baseScale * scaler[num - 1];

            for (int i = 0; i < num; i++)
            {
                Card card = Instantiate(prefabCard, baseGridCards.transform);
                card.Init(scaler[num - 1]);
                card.Complete += CompleteCards;
                cards.Add(card);
            }
            countCards = num;
        }

        public Card[] GetCards()
        {
            return cards.ToArray();
        }

        public void DestroyCards()
        {
            for (int i = 0; i < cards.Count; i++)
            {
                Destroy(cards[i].gameObject);
            }
            cards.Clear();
        }

        private void CompleteCards(Card card)
        {
            card.Complete -= CompleteCards;
            countCards--;
            if (countCards == 0)
                Bingo.Base.ListTabs.GetTab<GameTab>().FinishGame();
        }
    }
}