using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bingo.Base;
using Bingo.Menu;
using Bingo.Game.Panel;

namespace Bingo.Game
{
    public class GameTab : Tab
    {
        public RandomBall Balance { get { return randomBall; } }
        public GeneratorBall Generator { get { return generatorBall; } }

        public PanelControll Panel { get { return panelControll; }  }

        [SerializeField] private UIGameMaterial gameMaterial;

        [SerializeField] private GeneratorBall generatorBall;

        [SerializeField] private ControllCards controllCards;

        [SerializeField] private RandomBall randomBall;

        [SerializeField] private PanelControll panelControll;

        private void Awake()
        {
            gameMaterial.Exit.onClick.AddListener(delegate { FinishGame(); });
            TimerGame.EndTime += FinishGame;
        }

        public void StartGame(int countCard)
        {        
            controllCards.CreateCards(countCard);
            ScoreGame.ResetScore();
            TimerGame.StartTimer();

            List<Cell> cells = new List<Cell>();
            Card[] cards = controllCards.GetCards();
            for (int i = 0; i < cards.Length; i++) {
                cells.AddRange(cards[i].GetCells());
            }
            randomBall.BalanceRandom(cells.ToArray(),10*countCard);
            generatorBall.StartGame();
        }

        protected override void CallClose()
        {
            controllCards.DestroyCards();
        }

        public void FinishGame()
        {
            Panel.LoadPanel(Panel.PanelGameOver);
            TimerGame.StopTimer();

            generatorBall.ResetGame();
        }
    }
}