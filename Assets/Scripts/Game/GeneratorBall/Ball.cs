using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Bingo.Base;
using Bingo.Game;

namespace Bingo.Game
{
    public class Ball : MonoBehaviour
    {
        [SerializeField] private Text numBall;

        [SerializeField] private Text wordBall;

        [SerializeField] private Image circleOutline;

        public int NumBall { get; private set; }
        public char CollWord { get; private set; }

        public void InitBall(int num, char collWord) {
            NumBall = num;
            CollWord = collWord;
            numBall.text = num.ToString();
            wordBall.text = collWord.ToString();
        }

        public void SetScale(float scale) {
            transform.GetComponent<RectTransform>().localScale = new Vector3(scale,scale,scale);
        }

        public void StartAnim(float time) {
            circleOutline.fillAmount = 0;
            StartCoroutine(CircleOutlineAnim(time));
        }

        public bool IsNoEndAnim() {
            return circleOutline.fillAmount < 1f;
        }

        IEnumerator CircleOutlineAnim(float time) {
            float t = time / 100;
            for (int i = 1; i <= 100; i++) {
                circleOutline.fillAmount = (float)i / 100f;
                yield return new WaitForSeconds(t);
            }
        }
    }
}