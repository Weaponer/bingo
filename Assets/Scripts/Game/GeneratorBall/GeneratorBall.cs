using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Bingo;
using Bingo.Base;
using System.Linq;

namespace Bingo.Game
{
    public class GeneratorBall : MonoBehaviour
    {
        [SerializeField] private Ball prefabBall;

        [SerializeField] private RectTransform baseLine;

        [SerializeField] private RectTransform startPoint;
        [SerializeField] private RectTransform moveToPoint;
        [SerializeField] private RectTransform endPoint;


        private List<Ball> balls = new List<Ball>();

        bool isStart;

        Coroutine addBalls;
        Coroutine addB;

        GameTab gameTab;
        public void StartGame()
        {
            isStart = true;
            addBalls = StartCoroutine(AddBalls());
            gameTab = ListTabs.GetTab<GameTab>();
        }

        public bool CheckIsBall(int num, char word)
        {
            for (int i = 0; i < balls.Count; i++)
            {
                if (balls[i].NumBall == num && balls[i].CollWord == word)
                    return true;
            }
            return false;
        }

        public int GetScore(Cell cell)
        {
            for (int i = 0; i < balls.Count; i++)
            {
                if (balls[i].NumBall == cell.Num && balls[i].CollWord == cell.Word || cell.IsBust)
                {
                    if (balls[i].IsNoEndAnim())
                    {
                        return 50;
                    }
                    else
                    {
                        return 25;
                    }
                }
            }

            return -25;
        }

        public void ResetGame()
        {

            isStart = false;
            for (int i = 0; i < balls.Count; i++)
            {
                Destroy(balls[i].gameObject);
            }
            balls.Clear();
            if (addBalls != null)
            {
                StopCoroutine(addBalls);
                addBalls = null;
            }
            if (addB != null)
            {
                StopCoroutine(addB);
                addB = null;
            }
        }

        IEnumerator addBall()
        {
            yield return null;
            int num = 0;
            char word = ' ';
            bool t = false;
            while (!t)
            {
                gameTab.Balance.GetRandom(out num, out word);
                if (balls.Where((x) => x.NumBall == num && x.CollWord == word).FirstOrDefault() == null)
                {
                    t = true;
                }
            }

            Ball ball = Instantiate(prefabBall, baseLine);
            ball.InitBall(num, word);
            balls.Add(ball);
            ball.transform.position = startPoint.position;
            ball.SetScale(0);


            for (int i = 1; i <= 50; i++)
            {
                ball.SetScale((float)i / (float)50);
                yield return new WaitForSeconds(0.02f);
            }


            ball.StartAnim(2f);
            yield return new WaitForSeconds(2);

            if (balls.Count > 3)
            {
                float moveT = (ball.GetComponent<RectTransform>().rect.width * 2f + 30) * (Screen.width / 1080f);
                Vector3 dir = new Vector3(-moveT, 0, 0) / 25;
                Ball endB = balls[0];
                balls.Remove(endB);
                for (int i = 0; i < 25; i++)
                {
                    endB.SetScale(1f - (float)i / 25f);
                    for (int i2 = 0; i2 < balls.Count - 1; i2++)
                    {
                        balls[i2].transform.position += dir;
                    }
                    yield return new WaitForSeconds(0.01f);
                }
                Destroy(endB.gameObject);
            }
            else if (balls.Count > 0)
            {
                float moveT = (ball.GetComponent<RectTransform>().rect.width * 2f + 30) * (Screen.width / 1080f);
                Vector3 dir = new Vector3(-moveT, 0, 0) / 25f;
                for (int i = 0; i < 25; i++)
                {
                    for (int i2 = 0; i2 < balls.Count - 1; i2++)
                    {
                        balls[i2].transform.position += dir;
                    }
                    yield return new WaitForSeconds(0.01f);
                }
            }
            for (int i = 0; i < 25; i++)
            {
                ball.transform.position = Vector3.Lerp(startPoint.position, moveToPoint.position, (float)i / 25f);
                yield return new WaitForSeconds(0.01f);
            }

            addB = null;
        }

        IEnumerator AddBalls()
        {
            while (true)
            {
                addB = StartCoroutine(addBall());
                yield return new WaitForSeconds(5f);
            }
        }
    }
}