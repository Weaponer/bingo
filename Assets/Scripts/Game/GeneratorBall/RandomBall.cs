using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bingo.Game
{
    public class RandomBall : MonoBehaviour
    {
        List<int> randomNum = new List<int>();
        List<char> randomChar = new List<char>();

        public void BalanceRandom(Cell[] cells, int countRand) {
            randomNum.Clear();
            randomChar.Clear();
            for (int i = 0; i < cells.Length; i++) {
                randomNum.Add(cells[i].Num);
                randomChar.Add(cells[i].Word);
            }
            for (int i = 0;i < countRand;i++) {
                randomNum.Add(Random.Range(1,100));
                randomChar.Add(WordsBingo.Words[Random.Range(0, 5)]);
            }
        }

        public void GetRandom(out int num, out char word) {
            int i = Random.Range(0, randomChar.Count);
            num = randomNum[i];
            word = randomChar[i];
        }
    }
}