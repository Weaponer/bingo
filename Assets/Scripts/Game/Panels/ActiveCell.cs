using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Bingo.Base;
using Bingo.Menu;

namespace Bingo.Game.Panel
{
    public class ActiveCell : Panel
    {
        [SerializeField] private GridLayoutGroup gridLayout;

        [SerializeField] private ClickBall prefabBall;

        List<ClickBall> balls = new List<ClickBall>();

        List<Cell> pointOnCell = new List<Cell>();

        Card card;

        public void SetCells(Card card) {
            this.card = card;
            pointOnCell.Clear();
            Cell[] cells = card.GetCells();
            List<int> indexActive = new List<int>();
            for (int i = 0; i < cells.Length; i++) {
                if (!cells[i].IsBust && !cells[i].IsTrue) {
                    indexActive.Add(i);
                }
            }

            int max = Mathf.Min(4, indexActive.Count);
            while (pointOnCell.Count < max) {
                bool t = false;
                while (!t) {
                    int num = Random.Range(0, indexActive.Count);
                    if (pointOnCell.IndexOf(cells[num]) == -1) {
                        t = true;
                        pointOnCell.Add(cells[num]);
                    }
                }       
            }
        }

        protected override void CallOpen()
        {
            if (pointOnCell.Count == 0) {
                GameTab gameTab = ListTabs.GetTab<GameTab>();
                gameTab.Panel.ClosePanel();
                return;
            }

            for (int i = 0; i < pointOnCell.Count; i++) {
                ClickBall ball = Instantiate(prefabBall, gridLayout.transform);
                ball.Init(pointOnCell[i]);
                ball.InitBall(pointOnCell[i].Num, pointOnCell[i].Word);
                ball.Click += Click;
                balls.Add(ball);
            }
            
        }

        private void Click(ClickBall ball) {
            card.ActiveCell(ball.SaveCell);

            GameTab gameTab = ListTabs.GetTab<GameTab>();
            gameTab.Panel.ClosePanel();
        }

        protected override void CallClose()
        {
            for (int i = 0; i < balls.Count; i++) {
                Destroy(balls[i].gameObject);
            }

            balls.Clear();
        }
    }
}