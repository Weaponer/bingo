using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Bingo.Game.Panel
{
    public delegate void BallToClik(ClickBall ball);
    public class ClickBall : Ball
    {
        public event BallToClik Click;

        [SerializeField] private Button button;

        public Cell SaveCell { get; private set; }

        public void Init(Cell saveCell)
        {
            SaveCell = saveCell;
            button.onClick.AddListener(ToClick);
        }



        private void ToClick() {
            if (Click != null) {
                Click(this);
            }
        }
    }
}