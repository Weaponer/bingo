using Bingo.Base;
using Bingo.Menu;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bingo.Game.Panel
{
    public class GameOver : Panel
    {
        [SerializeField] private UnityEngine.UI.Button okButton;

        [SerializeField] private UnityEngine.UI.Text scoreText;

        protected override void CallOpen()
        {
            okButton.onClick.AddListener(Click);
            scoreText.text = "Score: " + ScoreGame.GetScore();
        }

        protected override void CallClose()
        {
            okButton.onClick.RemoveListener(Click);
        }

        private void Click() {
            GameTab game = ListTabs.GetTab<GameTab>();
            game.Panel.ClosePanel();
            TabsControll.OpenTab(ListTabs.GetTab<MenuTab>());
        }
    }
}