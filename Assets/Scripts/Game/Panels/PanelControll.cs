using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bingo.Game.Panel
{
    public class PanelControll : MonoBehaviour
    {
        [SerializeField] private GameOver gameOver;

        [SerializeField] private ActiveCell activeCell;

        public GameOver PanelGameOver { get { return gameOver; } }

        public ActiveCell PanelActiveCell { get { return activeCell; } }

        Panel oldPanel;

        public void LoadPanel(Panel panel) {
            if (oldPanel)
                oldPanel.Close();
            panel.Open();

            oldPanel = panel;
        }

        public void ClosePanel() {
            if (oldPanel)
                oldPanel.Close();
        }
    }
}