using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Bingo.Game
{
    public class ScoreGame : MonoBehaviour
    {
        public static ScoreGame Singleton { get; private set; }

        [SerializeField] private Text score;

        int s;

        private void Awake()
        {
            if (Singleton)
            {
                Destroy(gameObject);
                return;
            }
            else
            {
                Singleton = this;
            }
        }

        public static void ResetScore() {
            Singleton.score.text = "0";
            Singleton.s = 0;
        }

        public static int GetScore() { 
        return Singleton.s;
        }

        public static void AddScore(int score) {
            if (Singleton) {
                Singleton.s += score;
                Singleton.s = Mathf.Max(0, Singleton.s);
                Singleton.score.text = Singleton.s.ToString();
            }
        }

        private void OnDisable()
        {
            if (Singleton == this)
                Singleton = null;
        }
    }
}