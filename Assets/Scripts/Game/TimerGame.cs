using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerGame : MonoBehaviour
{
    public static TimerGame Singleton { get; private set; }

    public static event System.Action EndTime;

    [SerializeField] private Text text;

    Coroutine coroutine;

    int timer;

    private void Awake()
    {
        if (Singleton)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            Singleton = this;
        }
    }

    public static void StartTimer()
    {
        if (Singleton)
        {
            if (Singleton.coroutine != null)
                StopTimer();

            Singleton.coroutine = Singleton.StartCoroutine(Singleton.Timer());
        }
    }

    public static void StopTimer()
    {
        if (Singleton)
        {
            if (Singleton.coroutine != null)
                Singleton.StopCoroutine(Singleton.coroutine);
        }
    }

    public static void AddTime(int time) {
        Singleton.timer += time;
    }

    IEnumerator Timer()
    {
        timer = 120;
        while (timer > 0)
        {
            timer--;
            text.text = timer.ToString();
            yield return new WaitForSeconds(1);
        }
        if (EndTime != null)
            EndTime();
        coroutine = null;
    }

    private void OnDisable()
    {
        if (Singleton == this)
            Singleton = null;
    }
}
