using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bingo.Game
{
    public static class WordsBingo
    {
        public static readonly char[] Words = { 'B', 'I', 'N', 'G', 'O' };
    }
}