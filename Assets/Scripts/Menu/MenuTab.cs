using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bingo.Base;
using Bingo.Game;

namespace Bingo.Menu
{
    public class MenuTab : Tab
    {
        [SerializeField] private UIMenuMetarial menuMetarial;

        private void Awake()
        {
            menuMetarial.StartOneCard.onClick.AddListener(delegate { LoadGame(1); });
            menuMetarial.StartTwoCard.onClick.AddListener(delegate { LoadGame(2); });
            menuMetarial.StartThreeCard.onClick.AddListener(delegate { LoadGame(3); });
            menuMetarial.StartFourCard.onClick.AddListener(delegate { LoadGame(4); });           
        }

        private void Start()
        {
            TabsControll.OpenTab(this);
        }

        private void LoadGame(int countCards) {
            GameTab game = ListTabs.GetTab<GameTab>();
            TabsControll.OpenTab(game);
            game.StartGame(countCards);
        }
    }
}