using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Bingo.Base;

namespace Bingo.Menu
{
    public class UIMenuMetarial : UIMaterial
    {
        public Button StartOneCard;
        public Button StartTwoCard;
        public Button StartThreeCard;
        public Button StartFourCard;
    }
}
